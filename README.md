# FZM
Flashable zip maker from GSI (boot.img + system.img, in our case, for UBPorts) for CI by @sams3arson

Pass `out` folder with `boot.img` and `system.img` as first argument to script:
```bash
$ ./flashable_zip.sh $OUT
```

It will create flashable zip in your `out` folder.

> Important: place your own flashable zip sample ([our RMX2020 here](https://gitlab.com/ubport_rmx2020/ubports_RMX2020_zip.git)) in script, specific for each device
