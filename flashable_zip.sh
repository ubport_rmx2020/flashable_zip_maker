#!/bin/bash

# Flashable zip maker from GSI (boot.img + system.img, in our case, for UBPorts) for CI by @sams3arson

set -xe

echo "Flashable zip maker (@sams3arson) started..."

SCRIPT_DIR=$(dirname $(realpath $0))
OUT=$1

[ ! -z "$OUT" ] && [ -d "$OUT" ] || (echo "out directory is not passed or doesn't exist, exiting..." && exit -1)

[ -f "$OUT/system.img" ] && [ -f "$OUT/boot.img" ] || (echo "Either system.img or boot.img not found in provided out directory, exiting.." && exit -1)

cd $OUT
OUT=$(pwd -P)

TMP=$(mktemp -d) && cd "$TMP"

git clone https://gitlab.com/ubport_rmx2020/img2sdat.git && git clone https://gitlab.com/ubport_rmx2020/ubports_RMX2020_zip.git && rm -rf ubports_RMX2020_zip/.git

IMG2SDAT="$TMP/img2sdat"
FLASHZIP="$TMP/ubports_RMX2020_zip"
IMAGES="$TMP/preparing_images"
VENDOR_LINK="https://master.dl.sourceforge.net/project/firefly-files/Vendors-RMX2020/ubport_prebuild_vendor_a59.zip?viasf=1"
VENDOR_FILENAME="vendor.zip"
SYSTEM_FILES="system.new.dat.br system.patch.dat system.transfer.list"
VENDOR_FILES="vendor.new.dat.br vendor.patch.dat vendor.transfer.list"

mkdir "$IMAGES" && cd "$IMAGES"

python3 "$IMG2SDAT/img2sdat.py" "$OUT/system.img" "$IMAGES" 4

cd "$IMAGES"

brotli -6 "system.new.dat"

for checkfile in $SYSTEM_FILES; do
    [ -f "$checkfile" ] || (echo "img2sdat and brotli have been run, but not all required files are present, exiting..." && exit -1)
done

cd "$TMP"

curl "$VENDOR_LINK" -o "$VENDOR_FILENAME"

unzip "$VENDOR_FILENAME" -d "$FLASHZIP"

for checkfile in $VENDOR_FILES; do
    [ -f "$FLASHZIP/$checkfile" ] || (echo "vendor zip has been downloaded and unpacked, but vendor images can't be found, exiting..." && exit -1)
done

cp "$IMAGES/system.new.dat.br" "$IMAGES/system.patch.dat" "$IMAGES/system.transfer.list" "$OUT/boot.img" "$FLASHZIP"

[ -f "$SCRIPT_DIR/logo.img" ] && cp "$SCRIPT_DIR/logo.img" "$FLASHZIP" || (echo "logo missing, can't include it in zip" && exit -1)

cd "$FLASHZIP" && zip -r9 "$TMP/ubports_RMX2020_flashable.zip" *

cp "$TMP/ubports_RMX2020_flashable.zip" "$OUT"

echo "Flashable zip maker (@sams3arson) finished."
